jQuery(document).ready(function($) {

  // Show the login dialog box on click
  var $body = $('body');  //Store the body in a variable so we don't have to keep selecting it which will improve performance
  var $formLogin = $('form#login'); //Store the form too so we don't have to keep selecting it

  // $('.login_button').on('click', function(e){ //When the login button is clicked...
  //.preventDefault();//prevent the default action
  $formLogin.fadeIn(1000);//Fade the form in
  $('form#login a.close').on('click', function(e){
    e.preventDefault();
    $('form#login').fadeOut(1000);
  });
  //});


  // Perform AJAX login on form submit
  $('form#login').on('submit', function(e){
    $('form#login p.status').show().html(ajax_login_object.loadingmessage);
    $.ajax({
      type: 'POST',
      dataType: 'json',
      url: ajax_login_object.ajaxurl,
      data: {
        'action': 'ajaxlogin', //calls wp_ajax_nopriv_ajaxlogin
        'username': $('form#login #username').val(),
        'password': $('form#login #password').val(),
        'security': $('form#login #security').val() },
      success: function(data){
        $('form#login p.status').html(data.message);
        if (data.loggedin == true){
          document.location.href = ajax_login_object.redirecturl;
        }
      }
    });
    e.preventDefault();
  });

});