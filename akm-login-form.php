<?php
/*
Plugin Name: Akm Login Form
Plugin URI: http://lagovskiy.name/plugins
Description: This plugin shows login form on every page/post.
Version: 1.0
Author: Andrew Lagovskiy
Author URI: http://lagovskiy.name
*/


function akm_load_form() {
	if ( !is_user_logged_in() ) :
		?>
		<a href="#login" class="login_button">Log In</a>
		<form id="login" action="<?php esc_url( $_SERVER['REQUEST_URI'] ) ?>" method="post">
			<p class="status"></p>
			<h3 class="h1"><?php _e( 'Login To', 'blueleaf' ) ?></h3>

			<label for="username"><?php _e( 'Email', 'blueleaf' ); ?></label>
			<input id="username" name="username" type="text">
			<label for="password"><?php _e( 'Password', 'blueleaf' ); ?></label>
			<input id="password" name="password" type="password">

			<button name="submit" type="submit" value="<?php _e( 'Login', 'blueleaf' ) ?>"><?php _e( 'Login', 'blueleaf' ) ?></button>

			<a class="register small" href="<?php bloginfo( 'url' ); ?>/register/"><?php _e( 'Register...', 'blueleaf' ); ?></a><br>
			<a class="lost small" href="<?php echo wp_lostpassword_url(); ?>"><?php _e( 'Lost your password?', 'blueleaf' ); ?></a>

			<?php wp_nonce_field( 'ajax-login-nonce', 'security' ); ?>

		</form>
	<?php
	endif;
}

	add_action( 'wp_footer', 'akm_load_form' );

//AJAX Login scripts
add_action( 'wp_enqueue_scripts', 'load_ajax_login_script', 999 );

function load_ajax_login_script(){

	wp_enqueue_script( 'ajax-login-script',  plugin_dir_url( __FILE__ ) . 'ajax-login-script.js', array( 'jquery' ) );

	//wp_enqueue_script( 'jquery' );

	wp_localize_script( 'ajax-login-script', 'ajax_login_object', array(
			'ajaxurl'        => admin_url( 'admin-ajax.php' ),
			'redirecturl'    => $_SERVER['REQUEST_URI'],
			'loadingmessage' => __( 'Signing in, please wait...' )
	) );

	wp_enqueue_style('css-login-style', plugin_dir_url( __FILE__ ) . 'akm-login-style.css' );

}

add_action( 'wp_ajax_nopriv_ajaxlogin', 'ajax_login' );

function ajax_login(){

	// First check the nonce, if it fails the function will break
	check_ajax_referer( 'ajax-login-nonce', 'security' );

	// Nonce is checked, get the POST data and sign user on
	$info = array();
	$info['user_login'] = $_POST['username'];
	$info['user_password'] = $_POST['password'];
	$info['remember'] = true;

	$user_signon = wp_signon( $info, false );
	if ( is_wp_error($user_signon) ){
		echo json_encode(array('loggedin'=>false, 'message'=>__('Wrong username or password.')));
	} else {
		echo json_encode(array('loggedin'=>true, 'message'=>__('Login successful, redirecting...')));
	}
	die();
}

?>